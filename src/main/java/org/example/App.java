package org.example;

import java.util.Scanner;

/*
 Мои первые шаги в программировании на Java
 */
//TODO Написать код
public class App 
{
    public static void main( String[] args )
    {
        // Домашка к уроку 4 (задание 1)
        //==========================================================================
/*        int year = 0, result = 0;
        byte month = 0;
        boolean vvod = true;
        Scanner in = new Scanner(System.in);
        while (vvod){
            System.out.print("Введине месяц:");
            month = in.nextByte();
            if (month < 1 || month > 12) {
                System.out.println("Вы ввели некоректный месяц!!! Значение должно быть в диапазоне от 1 до 12!!!");
            }
            else {
                vvod = false;
            }
        }
        vvod = true;
        while (vvod){
            System.out.print("Введите год:");
            year = in.nextInt();
            if (year < 1900 || year > 9999 ) {
                System.out.println("Введите значение года в диапазоне 1900 до 9999 включительно!!!");
            }
            else {
                vvod = false;
            }

        }
        System.out.println(daysCount(month , year));

 */
/*
        // Домашка к уроку 4 (задание 2)
        //==========================================================================
        Scanner in = new Scanner(System.in);
        System.out.print("Введине число:");
        int n = in.nextInt();
        if (n < 2)
            System.out.println(n + " не входит в диапазон обрабатываемого числа!!!");
        else {
            if (isSimple(n) == 0) System.out.println("0");
            else System.out.println("Наименьший делитель " + isSimple(n));
        }
    }

    public static int isSimple( int n){
        if (n == 3)
            n = 0;
        if (n == 4)
            n = 2;
        for (int i = 2; i < Math.sqrt(n); i++) {
            if (n % i == 0) {
                n =  i;
                break;
            } else if (i == (int) Math.sqrt(n)){
                n = 0;
            };
        }
        return n;
    }

 */
        // Домашка к уроку 4 (задание 3)
        //==========================================================================
     int[] arr = {1,23,456,7070,10099,1001,1212121212};
     System.out.println(maxDigitsSumPosition(arr));
}
    public static byte maxDigitsSumPosition(int[] arr){
        int perMas, znach1 = 0, znachPr = 0;
        byte indexOfMax= 0;
        for (int i = 0; i < arr.length; i++) {
            int sumMass = 0;
            perMas = arr[i];
            while (perMas != 0) {
                sumMass += perMas % 10;
                perMas /= 10;
                if (perMas <= 0)
                    znachPr = sumMass;
            }
            if (znachPr >= znach1) {
                znach1 = znachPr;
                indexOfMax = (byte) i;
            }
        }
        return indexOfMax;
    }
        //==========================================================================
        // Урок 4
    /*    int[] nums = new int[6];

        for (int i = 0; i < 6; i++ ){
            nums[i] = i + 1;
        }
        for (int i: nums) {
            System.out.println(i);
        }
        //int nums[];
        //nums = new int[5];
        //int[] nums2 = new int[] {1,2,3,4,5};
        /*
        int[] nums3 = {1,2,3,4,5,6};
        nums3[3] = 10;
        nums3[1] = 20;
        for (int i: nums3){
            System.out.println(i);
        }

         */
/*
        for (int i = 10; i <= 20; i++){
            String str = "";
            for (int j = 10; j <= 20; j++){
                str += i * j + " ";
            }
            System.out.println(str);
        }
 */
        // Цикл со счетчиком

 /*       for (int i = 0; i <= 5; i++){
            System.out.println(i);
        }
*/
        // Цикл с предусловием
/*        int i = 0;
        while (i < 5){
            System.out.println(i);
            i++;
        }
*/
        // Цикл с постусловием
 /*
        int i = 0;
        do {
            System.out.println(i);
            i++;
        }
            while (i < 5);
*/


/*
        System.out.println(sayHello( "Я ", 5, false));
        System.out.println(getSeason(10));

        //==========================================================================
        // Урок 3
        int x = 15 % 4; // вернет остаток от деления - 3
        System.out.println( "Остаток от деления 15/4: " + x );
        x = 33;
        int y = 33;
        System.out.println( "Значение Х= " + x++ );
        System.out.println( "Значение Y= " + ++y );

        // "==" - сравнение
        // "!=" - неравно
        System.out.println( "Работаем с переменной X =" + x + " и переменной Y =" + y );
        System.out.println( "Равны ли переменные X и Y? - " + (x == y) );

        // "&&" - оператор "И"
        // "||" - оператор "ИЛИ"
        boolean t = true;
        boolean f = false;
        System.out.println( t && f);
        System.out.println( t || f);
        System.out.println("-----------------------------------");
        System.out.println( !t );
        System.out.println( !f );
        System.out.println("-----------------------------------");
        System.out.println( x = x + 10 );
        System.out.println( y += 20 ); // сокращенная запись от верхний строки (остальные операции можно использовать также).
        String s = x > y ? "Больше" : "Меньше"; // "?" - IF истина ":" - иначе
        System.out.println( "X " + s + " Y");
        // ( x || Y ) && ( a || b ) - скобки нужны для приоритета вычисления. "ИЛИ" - приоритетней "И"
        // x || Y && a || b - в данном случае нет.
        System.out.println("-----------------------------------");
        mySubProgramA(); // вызов подпрограммы
        mySubProgramB("Передаем переменную!");
        mySubProgramB("А=1000");
        sayHello("Java - ", 8 );
        sayHello("Народ - ", 1000 );
        System.out.println( sayHelloRet("Java - ", 8 ) );
        System.out.println( sayHelloRet("Народ - ", 1000 ) );
        System.out.println(factorial(5));
        // Домашка - последовательность Фибоначи
        byte fb = 12;
        System.out.println( "Число " + fibonazzi( fb ) );
        //=======================================================
        /* Урок 2
        String str1 = "Делаю первые шаги";
        String str2 = " в программировании на Java.";
        String str3;

        byte a = 10;
        short b = 40;
        int c = 8;
        long d = 50;
        long d1 = 888;
        long dp;

        float p = 10.5F;
        double l = 20.7;

        char h = 's';
        boolean z = true;

        // a = (byte)  d; // явное преобразование типа (cast)

        String dogName = "Тузик";

        str3 = dogName+"у " + a + " лет?";

        System.out.println( "Привет МИР!" );
        System.out.println("Я учусь программировать на языке Java!!!");
        System.out.println(str1+str2);
        System.out.println(str3);
        System.out.println("Нет, ему " + c + "!!!" );
        System.out.println("-----------------------------------");
        // Домашка
        System.out.println("А теперь немножечко МАГИИ!!!");
        System.out.println("Переменная а = " + d + ", а переменная d = " + d1 );
        System.out.println("Сим салябим");
        dp = d;
        d = d1;

        d1 = dp;
        System.out.println("Переменная а = " + d + ", а переменная d = " + d1 );
        System.out.println("-----------------------------------");
 */

 /*   public static byte daysCount(byte month, int year) {
        byte result = 0;
        switch (month) {
            case 1, 3, 5, 7, 8, 10,12:
                result = 31;
                break;
            case 4, 6, 9, 11:
                result = 30;
                break;
            case 2:
                if ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0)) {
                    result = 29;
                    break;
                }
                result = 28;

                break;
        }
        return result;
    }

  */
    /*
    public static String sayHello(String msg, int num, boolean flag){
        if (flag){
            return msg + "пришел " + num + " назад.";
        }
         return msg + "ушел " + num+ " назад";

    }
    public static String getSeason(int month){
        String result = "";
        switch (month){
            case 1:
            case 2:
            case 12:
                result = "Зима";
                break;
            case 3:
            case 4:
            case 5:
                result = "Весна";
                break;
            case 6:
            case 7:
            case 8:
                result = "Лето";
                break;
            case 9:
            case 10:
            case 11:
                result = "Осень";
                break;
            default:
                result = "Неизвестное время года";
        }
        return result;
    }

    public static long fibonazzi(int fb)
    {
        return  fb <= 1 ? fb : fibonazzi(fb - 1) + fibonazzi( fb - 2 )  ;
    }
    public static void mySubProgramA()
    {
        System.out.println("Привет, Java!");
        System.out.println("Привет, Народ!");
    }
    public static void mySubProgramB(String msg)
    {
        System.out.println( msg );

    }
    public static void sayHello(String msg, int num)
    {
        System.out.println( "Привет, " + msg + num );

    }
    public static String sayHelloRet(String msg, int num)
    {
    return "Возврат, " + msg + myCount(num) ;
    }
    public static int myCount(int num)
    {
        return num+10 ;
    }
    public static int factorial(int fk) // используем рекурсию при расчете факториала
    {
        return fk == 1 ? 1 : fk * factorial(fk - 1 ) ;
    }

     */
}
